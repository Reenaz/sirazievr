import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.PrettyXmlSerializer;
import org.htmlcleaner.TagNode;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Reenaz on 21.02.2017.
 */
public class ItemParser {
    HtmlCleaner cleaner = new HtmlCleaner();
    CleanerProperties props = cleaner.getProperties();
    NodeList nodes;
    NodeList nodes_img;
    NodeList nodes_price;
    XPathExpression expr;
    XPathExpression expr_url;
    XPathExpression expr_img;
    XPathExpression expr_price;
    XPath xpath;

    List<Item> items;
    List<String> urls;
    ItemDatabase itemDB;

    public List<String> linkParser(String url) throws IOException, ParserConfigurationException {
        TagNode tagNode = new HtmlCleaner(props).clean(new URL(url));

        props.setTranslateSpecialEntities(true);
        props.setTransResCharsToNCR(true);
        props.setOmitComments(true);

        new PrettyXmlSerializer(props).writeToFile(tagNode, "urls.xml", "utf-8");

        urls = new ArrayList<String>();

        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(true);
        DocumentBuilder builder = domFactory.newDocumentBuilder();

        XPathFactory factory = XPathFactory.newInstance();

        try{
            org.w3c.dom.Document doc = builder.parse("urls.xml");
            try{
                xpath = factory.newXPath();
                expr_url = xpath.compile("//li[@class='dropdown-submenu']/a[@tabindex='-1']/@href");

                Object result = expr_url.evaluate(doc, XPathConstants.NODESET);
                nodes = (NodeList) result;

                for (int i = 0; i < nodes.getLength(); i++) {
                    urls.add(nodes.item(i).getTextContent().trim());
                }
            }catch(Exception e){
                e.printStackTrace();
                System.out.println("Ошибка в XPath выражении!\n");
            }

        }catch(Exception e){
            System.out.println("Файл не найден");
        }
        return urls;

    }

    public List<String> getAllUrls(List<String> urls) throws IOException, ParserConfigurationException {
        List<String> allUrls = new ArrayList<String>();



        props.setTranslateSpecialEntities(true);
        props.setTransResCharsToNCR(true);
        props.setOmitComments(true);


        for(String url : urls){
            TagNode tagNode = new HtmlCleaner(props).clean(new URL(url));

            new PrettyXmlSerializer(props).writeToFile(tagNode, "AllUrls.xml", "utf-8");

            DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
            domFactory.setNamespaceAware(true);
            DocumentBuilder builder = domFactory.newDocumentBuilder();

            XPathFactory factory = XPathFactory.newInstance();
            try{
                org.w3c.dom.Document doc = builder.parse("Allurls.xml");
                try{
                    xpath = factory.newXPath();
                    expr_url = xpath.compile("//li[@class='dropdown-submenu']/a[@tabindex='-1']/@href");

                    Object result = expr_url.evaluate(doc, XPathConstants.NODESET);
                    nodes = (NodeList) result;

                    for (int i = 0; i < nodes.getLength(); i++) {
                        urls.add(nodes.item(i).getTextContent().trim());
                        allUrls.add(nodes.item(i).getTextContent().trim());

                    }
                }catch(Exception e){
                    e.printStackTrace();
                    System.out.println("Ошибка в XPath выражении!\n");
                }

            }catch(Exception e){
                System.out.println("Файл не найден");
            }

        }
        return allUrls;
    }

    public List<Item> GetItems(String url) throws IOException, ParserConfigurationException {
        TagNode tagNode = new HtmlCleaner(props).clean(new URL(url));

        props.setTranslateSpecialEntities(true);
        props.setTransResCharsToNCR(true);
        props.setOmitComments(true);

        new PrettyXmlSerializer(props).writeToFile(tagNode, "items.xml", "utf-8");

        items = new ArrayList<Item>();

        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(true);
        DocumentBuilder builder = domFactory.newDocumentBuilder();

        XPathFactory factory = XPathFactory.newInstance();


        try{
            org.w3c.dom.Document doc = builder.parse("items.xml");
            try{
                xpath = factory.newXPath();
                expr = xpath.compile("//div[@class='row-viewed col-catalog-grid product-grid']//div[@class='item-product-inner']/a[@class='item-link']");
                xpath = factory.newXPath();
                expr_img = xpath.compile("//div[@class='row-viewed col-catalog-grid product-grid']//div[@class='item-product-inner']/a[@class='item-link']/img/@src");
                xpath = factory.newXPath();
                expr_price = xpath.compile("//div[@class='row-viewed col-catalog-grid product-grid']//div[@class='item-product-inner']/span[@class='item-price']");

                Object result = expr.evaluate(doc, XPathConstants.NODESET);
                nodes = (NodeList) result;
                Object result_img = expr_img.evaluate(doc, XPathConstants.NODESET);
                nodes_img = (NodeList) result_img;
                Object result_price = expr_price.evaluate(doc, XPathConstants.NODESET);
                nodes_price = (NodeList) result_price;

                for (int i = 0; i < nodes.getLength(); i++) {
                    items.add(new Item(nodes.item(i).getTextContent().trim(), nodes_price.item(i).getTextContent().trim(), nodes_img.item(i).getTextContent().trim()));
                }


            }catch(Exception e){
                e.printStackTrace();
                System.out.println("Ошибка в XPath выражении!\n");
            }

        }catch(Exception e){
            System.out.println("Файл не найден");
        }
        return items;
    }


}
