/**
 * Created by Reenaz on 20.02.2017.
 */
public class Item {
    private String name;
    private String price;
    private String imgLink;

    public Item(String name, String price, String imgLink){
        this.name = name;
        this.price = price;
        this.imgLink = imgLink;
    }

    public String getName(){
        return name;
    }

    public String getPrice(){
        return price;
    }

    public String getImgLink(){
        return imgLink;
    }
}
