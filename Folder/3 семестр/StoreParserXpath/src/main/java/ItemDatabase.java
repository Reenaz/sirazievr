import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Reenaz on 20.02.2017.
 */
public class ItemDatabase {
    private static Connection conn;
    private static ResultSet rs;

    public static List<Item> getAllItems() throws SQLException {
        ArrayList<Item> items = new ArrayList<Item>();
        conn = DbConnector.getConection();
        ResultSet rs = null;
        rs = conn.createStatement().executeQuery("SELECT* FROM items");
        while (rs.next()) {
            items.add(new Item(
                    rs.getString("item_name"),
                    rs.getString("item_price"),
                    rs.getString("item_img")));
        }
        return items;
    }

    public static void addItem(Item item) throws SQLException {
        conn = DbConnector.getConection();
        PreparedStatement st = conn.prepareStatement(
                "INSERT INTO items (item_name, item_price, item_img)" +
                        "VALUES (?,?,?)");
        st.setString(1, item.getName());
        st.setString(2, item.getPrice());
        st.setString(3, item.getImgLink());
        st.execute();
    }

    public static void removeAllItems() throws SQLException {
        conn = DbConnector.getConection();
        conn.createStatement().executeUpdate("DELETE FROM items");
    }
}
