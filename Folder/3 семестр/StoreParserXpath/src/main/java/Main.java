import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.PrettyXmlSerializer;
import org.htmlcleaner.TagNode;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Reenaz on 14.02.2017.
 */
public class Main {
    public static void main(String[] args) throws IOException, ParserConfigurationException, SQLException {
        String url = "https://4frag.ru";
        List<Item> items = new ArrayList<Item>();
        List<String> urls = new ArrayList<String>();
        ItemDatabase itemDB = new ItemDatabase();

        ItemParser itemParser = new ItemParser();
        urls = itemParser.linkParser(url);

        ItemDatabase.removeAllItems();

        for(String u : urls){
           items = itemParser.GetItems(u);
            for(Item item : items){
                ItemDatabase.addItem(item);
            }
        }
    }


}

