import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Reenaz on 20.02.2017.
 */
public class DbConnector {
    private final static String DRIVER = "com.mysql.jdbc.Driver";
    private final static String CONNECTION_URI = "jdbc:mysql://localhost:3307/xpathstore";
    private final static String LOGIN = "Reenaz";
    private final static String PASSWORD = "Rinaz2010";

    private static Connection conn;

    public static Connection getConection() {
        if (conn == null) {
            try {
                Class.forName(DRIVER);
                conn = DriverManager.getConnection(CONNECTION_URI, LOGIN, PASSWORD);
            } catch (SQLException ex) {
                System.out.println("Can't connect to DB (" + ex.getErrorCode() + ": " + ex.getMessage() + ").");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
        return conn;

    }
}
