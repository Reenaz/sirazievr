package ru.kpfu.interfaces;

import ru.kpfu.entites.User;

import java.io.IOException;
import java.sql.SQLException;

public interface DataBaseInterface {
    public void addUser(User user) throws SQLException;
    public boolean checkLogin(String login) throws IOException, SQLException;
}
