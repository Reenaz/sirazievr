import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Reenaz on 26.12.2016.
 */
public class ORM {
    public static void create(String classname, Object cInstance) throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class c = Class.forName(classname);
        Method[] methods = c.getMethods();

        Connector connector = new Connector();
        List<String> methodsNames = new ArrayList<String>();
        List<Method> methodList = new ArrayList<Method>();
        for (Method method : methods) {
            if (method.getName().startsWith("get"))
                methodsNames.add(method.getName());
        }

        Connection conn = Connector.getConnection();
        PreparedStatement preparedStatement = null;
        for (int i = 0; i < methodsNames.size(); i++) {
            Method method = c.getMethod(methodsNames.get(i));
            methodList.add(method);
        }
        try {
            preparedStatement = conn.prepareStatement(
                    "INSERT INTO users(fullname, user_password, email, gender, city) VALUES(?,?,?,?,?)");
            preparedStatement.setString(1, String.valueOf(methodList.get(0).invoke(cInstance)));
            preparedStatement.setString(2, String.valueOf(methodList.get(1).invoke(cInstance)));
            preparedStatement.setString(3, String.valueOf(methodList.get(2).invoke(cInstance)));
            preparedStatement.setString(4, String.valueOf(methodList.get(3).invoke(cInstance)));
            preparedStatement.setString(5, String.valueOf(methodList.get(4).invoke(cInstance)));
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.print("");
        }
    }

        public static ArrayList<Object> getAll(String classname) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
            Class c = Class.forName(classname);
            HashMap<Field, String> fields = new HashMap<Field, String>();
            Field[] publicFields = c.getFields();
            for(Field f:publicFields){
                fields.put(f, null);
            }
            List<Object> objects = new ArrayList<Object>();
            Connector connector = new Connector();
            ResultSet rs;

            Connection conn = Connector.getConnection();

            try {
                rs = conn.createStatement().executeQuery("SELECT* FROM users");
                while (rs.next()) {
                    Object modelObject = c.newInstance();
                    fields.forEach((k, v) -> {
                        try {
                            Method rsMethod = rs.getClass().getMethod("get" + v, String.class);
                            k.set(modelObject, rsMethod.invoke(rs, k.getName()));
                        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    });
                    objects.add(modelObject);

                }

                return (ArrayList<Object>) objects;


            }catch (SQLException e){
                e.printStackTrace();
                return new ArrayList<Object>();
            }
        }
}
