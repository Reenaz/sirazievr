import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import static java.awt.image.AffineTransformOp.TYPE_BICUBIC;

/**
 * Created by IIMul on 02.12.2016.
 */
public class Window extends JFrame {
    private BufferedImage bufferedImage;
    private Graphics graphics;
    private Graphics2D graphics2D;
    private DrawPanel drawPanel;
    private Container container;
    private JButton clear, rotate, roll, reflect;
    private JTextField textField;
    private Timer timer;
    private AffineTransformOp affineTransformOp;
    private AffineTransform affineTransform;
    private double theta = 0;

    Window(String name) {
        super(name);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(100, 80, 650, 600);
        container = getContentPane();
        drawPanel = new DrawPanel();
        drawPanel.setBackground(Color.WHITE);
        container.add(drawPanel, BorderLayout.CENTER);
        createGUI();
        setResizable(false);
        setVisible(true);

    }

    private void createGUI() {
        JPanel grid = new JPanel();
        grid.setBounds(0, 0, 50, 600);
        grid.setLayout(new GridLayout(5, 1));
        JPanel flow = new JPanel();
        flow.setLayout(new FlowLayout(FlowLayout.LEFT));
        clear = new JButton("Clear");
        clear.setSize(new Dimension(45, 30));
        rotate = new JButton("Rotate");
        roll = new JButton("Roll");
        reflect = new JButton("Reflect");
        textField = new JTextField(10);
        grid.add(clear);
        grid.add(rotate);
        grid.add(textField);
        grid.add(roll);
        grid.add(reflect);
        flow.add(grid);
        container.add(flow, BorderLayout.WEST);
        addListener();
    }

    private void addListener() {

        clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (timer != null)
                    if (timer.isRunning())
                        timer.stop();
                drawPanel.clear();
            }
        });

        rotate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                affineTransformOp = new AffineTransformOp(AffineTransform.getRotateInstance(Double.parseDouble(textField.getText()), (drawPanel.getLastPoint().getX() + drawPanel.getStartPoint().getX()) / 2, (drawPanel.getLastPoint().getY() + drawPanel.getStartPoint().getY()) / 2), TYPE_BICUBIC);
                bufferedImage = affineTransformOp.filter(bufferedImage, null);
                drawPanel.repaint();
            }
        });

        roll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                timer = new Timer(1000/200, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        affineTransformOp = new AffineTransformOp(AffineTransform.getRotateInstance((2), drawPanel.getStartPoint().getX(), drawPanel.getStartPoint().getY()), TYPE_BICUBIC);
                        bufferedImage = affineTransformOp.filter(bufferedImage, null);
                        drawPanel.repaint();
                    }
                });
                timer.start();
            }
        });

        reflect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                affineTransform = new AffineTransform(-1.0, 0.0, 0.0, 1.0, bufferedImage.getWidth(), 0.0);
                affineTransformOp = new AffineTransformOp(affineTransform, AffineTransformOp.TYPE_BILINEAR);
                bufferedImage = affineTransformOp.filter(bufferedImage, null);

//                affineTransformOp = new AffineTransformOp(AffineTransform.getRotateInstance((Math.PI), drawPanel.getLastPoint().getX(), (drawPanel.getStartPoint().getY() + drawPanel.getLastPoint().getY())/2), TYPE_BICUBIC);
//
//                bufferedImage = affineTransformOp.filter(bufferedImage, null);

                drawPanel.repaint();
            }
        });

    }

    private class DrawPanel extends JPanel {
        private Point startPoint = new Point();
        private Point lastPoint = new Point();

        DrawPanel() {
            addListener();
        }

        private Point getLastPoint() {
            return lastPoint;
        }

        private Point getStartPoint() {
            return startPoint;
        }

        private void addListener() {
            addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    graphics = bufferedImage.getGraphics();
                    graphics2D = (Graphics2D) graphics;
                    graphics2D.setStroke(new BasicStroke(2));
                    graphics2D.setColor(Color.black);
                    graphics2D.drawLine(e.getX(), e.getY(), e.getX() + 1, e.getY() + 1);
                    drawPanel.repaint();
                }

                @Override
                public void mousePressed(MouseEvent e) {
                    clear();
                    startPoint.setX(e.getX());
                    startPoint.setY(e.getY());
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    lastPoint.setY(e.getY());
                    lastPoint.setX(e.getX());
                    graphics = bufferedImage.getGraphics();
                    graphics2D = (Graphics2D) graphics;
                    graphics2D.setStroke(new BasicStroke(2));
                    graphics2D.setColor(Color.black);
                    graphics2D.drawRect(startPoint.getX(), startPoint.getY(), lastPoint.getX() - startPoint.getX(), lastPoint.getY() - startPoint.getY());
                    repaint();
                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }

                @Override
                public void mouseExited(MouseEvent e) {

                }
            });


        }

        public void paintComponent(Graphics graphics) {
            if (bufferedImage == null) {
                bufferedImage = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics2D graphics2D = bufferedImage.createGraphics();
                graphics2D.setColor(Color.white);
                graphics2D.fillRect(0, 0, this.getWidth(), this.getHeight());
            }
            super.paintComponent(graphics);
            graphics.drawImage(bufferedImage, 0, 0, this);
        }

        private void clear() {
            if (timer != null)
                if (timer.isRunning())
                    timer.stop();
            Graphics graphics = bufferedImage.getGraphics();
            Graphics2D graphics2D = (Graphics2D) graphics;
            graphics2D.setColor(Color.WHITE);
            graphics2D.setStroke(new BasicStroke());
            for (int i = 0; i < this.getWidth(); i++)
                for (int j = 0; j < this.getHeight(); j++)
                    graphics2D.drawLine(i, j, i + 1, j + 1);
            this.repaint();
            graphics.dispose();
            graphics2D.dispose();
        }
    }

    private class Point {
        private int x;
        private int y;

        private int getX() {
            return x;
        }

        private void setY(int y) {
            this.y = y;
        }

        private int getY() {
            return y;
        }

        private void setX(int x) {
            this.x = x;
        }
    }
}
