public class ConvApp
{

	public static void main(String[] args)
	{
		ConvModel model = new ConvModel();
		ConvView view = new ConvView(model);
		
		view.setVisible(true);		
	}
}