import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ConvModel {
	
	public  final double CM_PER_INCH = 2.54;
	
	public  final double LBS_PER_KG = 2.2; ;
	
	private final double faren_const = 9.0/5.0;
	private final double faren_const2 = 32.0;
	private final double inverse_const = 5.0/9.0;
	private double inputValue;
	private double resultValue;

	private String first;

	private String second;

	public void cmToInches(double cm){
		inputValue = cm;
		resultValue = inputValue/this.CM_PER_INCH;
		collectStrings("cm", "inches");

	}

	public void inchesToCm(double in){
		inputValue = in;
		resultValue = this.CM_PER_INCH * inputValue;
		collectStrings("inches", "cm");
	}

	public void cToF(double c){
		inputValue =c;
		resultValue = (inputValue *this.faren_const) + this.faren_const2;
		collectStrings("\u00B0C", "\u00B0F");
	}

	public void fToC(double f){
		inputValue = f;
		resultValue = (inputValue - this.faren_const2)/this.inverse_const ;
		collectStrings("\u00B0F", "\u00B0C");
	}

	public double getInputValue(){
		return this.inputValue;

	}

	public double getResultValue(){
		return this.resultValue;

	}

	public void kgToLbs(double kgs){
		inputValue = kgs;
		resultValue = inputValue*this.LBS_PER_KG;
		collectStrings("kg", "Lbs");
	}


	public void lbsToKg(double lbs){
		inputValue = lbs;
		resultValue = inputValue/this.LBS_PER_KG;

		collectStrings("Lbs","Kg");
	}

	private void collectStrings(String first, String second){
		this.first = first; 
		this.second = second;
	}
	
	private String getFirststring(){
		return this.first;
	}
	private String getSecondstring(){
		return this.second;
	}
}