import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ConvView extends JFrame{

	private ConvModel model;
	private  JButton cm_to_inches;
	private  JButton kg_to_lb;
	private  JButton F_to_C;
	private  JButton inches_to_cm;
	private  JButton lb_to_kg;
	private  JButton C_to_F;
	private  JPanel p2;
	private  JPanel p1;
	private  JTextField inputField;
	private  JTextField resultfield;
	private JLabel input;
	private JLabel output;

	public ConvView(ConvModel model){
		super("Конвертер");
		
		this.model = model;

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(true);
		this.setVisible(true);
		this.setPreferredSize(new Dimension(800,400));
		this.setLocationRelativeTo(null);
		this.setLayout(new GridLayout(2,1));
		
		p1 = new JPanel();
		add(p1);
		p1.setLayout(new GridLayout(2, 2, 5, 0));

		input = new JLabel("                                                                                         Введите значение:");
		p1.add(input);
		inputField = new JTextField(20);
		inputField.setSize(20, 20);
		p1.add(inputField);

		output = new JLabel("                                                                                                        Результат:");
		p1.add(output);
		resultfield = new JTextField(20);
		resultfield.setEditable(false);
		p1.add(resultfield);

		
		p2 = new JPanel();
		add(p2);
		p2.setLayout(new GridLayout(3,2));
		
		
		C_to_F = new JButton("\u00B0C в F");
		p2.add(C_to_F);
		
		F_to_C  = new JButton("F в \u00B0C ");
		p2.add(F_to_C);
		
		cm_to_inches  = new JButton("см в дюймы");
		p2.add(cm_to_inches);
		
		inches_to_cm = new JButton("дюймы в см");
		p2.add(inches_to_cm);
		
		
		kg_to_lb  = new JButton("кг в фунты");
		p2.add(kg_to_lb);
		
		lb_to_kg  = new JButton("фунты в кг");
		p2.add(lb_to_kg);
		pack();
		 


		ConvController control = new ConvController(model, this);


	}

	public void addCmToInchesListener(ActionListener t){
		this.cm_to_inches.addActionListener(t);
		
	}

	public void addCtoFListener(ActionListener t){
		this.C_to_F.addActionListener(t);
	}

	public void addFtoCListener(ActionListener t){
		this.F_to_C.addActionListener(t);
	}

	public void addInchesToCmListener(ActionListener t){
		this.inches_to_cm.addActionListener(t);
	}

	public void addKgToLbsListener(ActionListener t){
		this.kg_to_lb.addActionListener(t);
	}
	
	public void addLbsToKgListener(ActionListener t){
		this.lb_to_kg.addActionListener(t);
	}


	public String getInputValue() {	return inputField.getText(); }

	public void setResultValue(String value){
		resultfield.setText(value);
	}

}