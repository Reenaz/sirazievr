import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

public class ConvController {
	

	private ConvModel model;
	private ConvView view;
	DecimalFormat format = new DecimalFormat();
	
	public ConvController(ConvModel model, ConvView view){
		this.model = model;
		this.view= view;
		
		
		this.view.addCmToInchesListener(new  CmToInchesListener());
		this.view.addInchesToCmListener(new InchesToCmListener());
		this.view.addKgToLbsListener(new KgToLbsListener());
		this.view.addLbsToKgListener(new LbsToKgListener());
		this.view.addCtoFListener(new CtoFListener());
		this.view.addFtoCListener(new FtoCListener());
		format.setMaximumFractionDigits(3);
	}

	
	public ConvModel getModel(){
		return this.model;
	}

	public ConvView getView(){
		return this.view;
	}

	public class FtoCListener extends UnitConvControllerListener {
		protected void doOperation(double valueInput){
			getModel().fToC(valueInput);
		}
	}

	private class CtoFListener extends UnitConvControllerListener {
		protected void doOperation(double valueInput){
			getModel().cToF(valueInput);
		}
		
	}

	private class LbsToKgListener extends UnitConvControllerListener {
		protected void doOperation(double valueInput){
			getModel().lbsToKg(valueInput);
			
		}
	}

	private class KgToLbsListener extends UnitConvControllerListener{
		protected void doOperation(double valueInput){

			getModel().kgToLbs(valueInput);	
			
		}
	}

	private class InchesToCmListener extends UnitConvControllerListener {
		protected void doOperation(double valueInput){
			getModel().inchesToCm(valueInput);
		}
	}

	private class CmToInchesListener extends UnitConvControllerListener {
		protected void doOperation(double valueInput){
			getModel().cmToInches(valueInput);
		}
		
	}
	
	private abstract class UnitConvControllerListener implements ActionListener
	{
		
		protected abstract void doOperation(double valueInput);
	
	
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Double InputFieldValue = 0.00;
			String temp;
			try
			{
				
				temp = getView().getInputValue();
				InputFieldValue = Double.parseDouble(temp);
			}

			catch(Exception ex)
			{
				InputFieldValue = Double.NaN;
			}
			
			doOperation(InputFieldValue);
			double modelresultvalue = getModel().getResultValue();
			getView().setResultValue(String.valueOf(format.format(modelresultvalue)));
		}
	}

}