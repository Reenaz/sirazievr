import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;


public class Window extends JFrame {

    private BufferedImage bufferedImage;
    private Graphics graphics;
    private Graphics2D graphics2D;
    private DrawPanel drawPanel;
    private Container container;

    Window(String name){
        super(name);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(100, 80, 650, 600);
        container = getContentPane();
        drawPanel = new DrawPanel();
        drawPanel.setBackground(Color.WHITE);
        container.add(drawPanel, BorderLayout.CENTER);
        createGUI();
        setResizable(false);
        setVisible(true);
    }

    private void createGUI() {
        JPanel grid = new JPanel();
        grid.setBounds(0, 0, 50, 600);
        grid.setLayout(new GridLayout(5, 1));
        JPanel flow = new JPanel();
        flow.setLayout(new FlowLayout(FlowLayout.LEFT));
        JButton clear = new JButton("Clear");
        clear.setSize(new Dimension(45, 30));
        grid.add(clear);
        flow.add(grid);
        container.add(flow, BorderLayout.WEST);
        clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphics2D = (Graphics2D) bufferedImage.getGraphics();
                graphics2D.setColor(Color.WHITE);
                graphics2D.setStroke(new BasicStroke());
                for (int i = 0; i < drawPanel.getWidth(); i++)
                    for (int j = 0; j < drawPanel.getHeight(); j++)
                        graphics2D.drawLine(i, j, i + 1, j + 1);
                drawPanel.repaint();
                graphics.dispose();
                graphics2D.dispose();
            }
        });
    }

    private class DrawPanel extends JPanel{

        private Point first = new Point();
        private Point second = new Point();
        private Point third = new Point();
        private Point fourth = new Point();
        private Point fifth = new Point();
//
        private DrawPanel(){
            this.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {

                }

                @Override
                public void mousePressed(MouseEvent e) {
                    first.set(e.getX(), e.getY());
                }

                @Override
                public void mouseReleased(MouseEvent e) {

                    int height = e.getY() - first.getY();
                    if (height>0) {
                        int width = (int) (height * Math.tan(Math.PI / 10));
                        third.set(first.getX() + width, e.getY());
                        fourth.set(first.getX() - width, e.getY());
                        int length = (int) Math.sqrt((third.getX() - first.getX()) * (third.getX() - first.getX()) + (first.getY() - third.getY()) * (first.getY() - third.getY()));
                        int edge = (int) (length / 2 / (1 + Math.sin(Math.PI / 10)));
                        int minWidth = (int) (edge * Math.sin(Math.PI / 10));
                        int minHeight = (int) (edge * Math.cos(Math.PI / 10));
                        second.set(first.getX() + minWidth + edge, first.getY() + minHeight);
                        fifth.set(first.getX() - minWidth - edge, first.getY() + minHeight);
                        graphics = bufferedImage.getGraphics();
                        graphics2D = (Graphics2D) graphics;
                        graphics2D.setStroke(new BasicStroke(2));
                        graphics2D.setColor(Color.black);
                        graphics2D.drawLine(first.getX(), first.getY(), third.getX(), third.getY());
                        graphics2D.drawLine(first.getX(), first.getY(), fourth.getX(), fourth.getY());
                        graphics2D.drawLine(second.getX(), second.getY(), fifth.getX(), fifth.getY());
                        graphics2D.drawLine(second.getX(), second.getY(), fourth.getX(), fourth.getY());
                        graphics2D.drawLine(third.getX(), third.getY(), fifth.getX(), fifth.getY());
                        repaint();
                    }
                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }

                @Override
                public void mouseExited(MouseEvent e) {

                }
            });
        }

        protected void paintComponent(Graphics graphics) {
            if (bufferedImage == null) {
                bufferedImage = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics2D graphics2D = bufferedImage.createGraphics();
                graphics2D.setColor(Color.white);
                graphics2D.fillRect(0, 0, this.getWidth(), this.getHeight());
            }
            super.paintComponent(graphics);
            graphics.drawImage(bufferedImage, 0, 0, this);
        }
    }

    private class Point {
        private int x = 0;
        private int y = 0;

        private int getX() {
            return x;
        }

        private int getY() {
            return y;
        }

        private void set(int x, int y){
            this.x = x;
            this.y = y;
        }
    }
}
