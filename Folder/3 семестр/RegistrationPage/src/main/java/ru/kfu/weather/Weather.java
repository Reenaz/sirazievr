package ru.kfu.weather;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Weather {
    public static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q=kazan,ru&units=metric&appid=7ab2e37a768d7d684ede2febe47de4b9";

    // открываем соедиение к указанному URL
    // помощью конструкции try-with-resources

    public String getWeather(){
        URL url = createUrl(WEATHER_URL);
        StringBuilder stringBuilder = new StringBuilder();
        try{
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String inputLine;
            // построчно считываем результат в объект StringBuilder
            while ((inputLine = in.readLine()) != null) {
                stringBuilder.append(inputLine);
                System.out.println(inputLine);
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
        String resultJson =  stringBuilder.toString();

        try {
            JSONObject weatherJsonObject = (JSONObject) JSONValue.parseWithException(resultJson);
            JSONObject weatherObject = (JSONObject) weatherJsonObject.get("main");

            return weatherObject.get("temp").toString();

        } catch (org.json.simple.parser.ParseException e) {
            return "0";
        }
    }



    public static URL createUrl(String link) {
        try {
            return new URL(link);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
