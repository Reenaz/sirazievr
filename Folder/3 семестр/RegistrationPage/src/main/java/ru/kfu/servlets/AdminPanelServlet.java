package ru.kfu.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Reenaz on 30.09.2016.
 */
public class AdminPanelServlet extends HttpServlet {
    private static String adminLogin = "Admin";
    private static String adminPassword = "123456";



    protected  void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/views/adminInput.jsp").forward(req, resp);
    }

    protected void doPost(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException {
        if(req.getParameter("login").equals(adminLogin) && req.getParameter("password").equals(adminPassword)){
            getServletContext().getRequestDispatcher("/WEB-INF/views/adminPanel.jsp").forward(req, resp);
        }
        else{
            resp.sendRedirect("/admin");
        }
    }
}
