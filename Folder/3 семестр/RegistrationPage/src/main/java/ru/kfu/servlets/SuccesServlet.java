package ru.kfu.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Reenaz on 29.09.2016.
 */

public class SuccesServlet extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)  throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/views/succes.jsp").forward(req, resp);
    }

}
