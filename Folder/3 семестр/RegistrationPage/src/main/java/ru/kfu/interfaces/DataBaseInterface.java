package ru.kfu.interfaces;

import java.io.IOException;

/**
 * Created by Reenaz on 29.09.2016.
 */
public interface DataBaseInterface {
    public void Writer(String[] message) throws IOException;
    public boolean Reader(String login) throws IOException;
    public boolean checkUser(String login, String password) throws IOException;
}
