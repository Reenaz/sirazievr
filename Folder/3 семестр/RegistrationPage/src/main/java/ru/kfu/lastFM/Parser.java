package ru.kfu.lastFM;

import org.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;

public class Parser {

    public String getSizeListener(String data) throws JSONException {
        JSONObject jsonObj = new JSONObject(data);
        JSONObject artistObj = jsonObj.getJSONObject("artist");
        JSONObject statsObj = artistObj.getJSONObject("stats");
        long playcount = statsObj.getLong("playcount");
        return String.valueOf(playcount);
    }

    public String getNameArtist(String data) throws JSONException {
        JSONObject jsonObject = new JSONObject(data);
        JSONObject artistObj = jsonObject.getJSONObject("artist");
        String nameArtist = artistObj.getString("name");
        return nameArtist;
    }
    public String getImgArtist(String data) throws JSONException {
        JSONObject jsonObject = new JSONObject(data);
        JSONObject artistObj = jsonObject.getJSONObject("artist");
        JSONArray imgArr = artistObj.getJSONArray("image");
        JSONObject imgObj = imgArr.getJSONObject(3);
        String img = imgObj.getString("#text");
        return img;
    }
}
