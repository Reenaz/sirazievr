package ru.kfu.servlets;

import ru.kfu.repositories.DataBase;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Reenaz on 29.09.2016.
 */
public class RegistrationServlet extends HttpServlet {
    public static DataBase db = new DataBase();
    public String name="" ;
    public String login="";
    public String password;
    public String repassword;
    public String gender = "-1";
    public String country;
    private String aboutYourself="";
    private String newsletter;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/views/registration.jsp").forward(req, resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        String[] data = new String[7];
        name = req.getParameter("fullname");
        login = req.getParameter("login");
        password = req.getParameter("password");
        repassword = req.getParameter("repassword");
        gender = req.getParameter("gender");
        country = req.getParameter("country");
        aboutYourself = req.getParameter("aboutYourself");
        newsletter = req.getParameter("newsletter");

        String errorNameEquals = "";
        String errorNameMatches = "";
        String errorLogin = "";
        String errorLoginMatches = "";
        String errorPassLength = "";
        String errorPassEquals = "";
        String errorGender = "";


        if(db.Reader(login)) errorLogin = "This login already taken";
        if(name.equals("")) errorNameEquals ="Enter the name";
        if(!name.matches("^[a-zA-Z\\s{,2}]{0,50}$")) errorNameMatches= "Name must contain valid characters";
        if(!login.matches("^[a-zA-Z0-9_@.-]{11,25}$")) errorLoginMatches= "Login must contain valid characters and have a length from 11 to 25";
        if(password.length()<6) errorPassLength="The password must be at least 6 characters";
        if(!password.equals(repassword)) errorPassEquals="Passwords don't match";
        if(gender!=null){
            if(gender.equals("-1"))
                errorGender = "Select gender";
        }
        else {
            errorGender = "Select gender";
            gender = "-1";
        }

        if(errorNameEquals.equals("") && errorGender.equals("") && errorNameMatches.equals("") && errorLogin.equals("") && errorLoginMatches.equals("") && errorPassLength.equals("") && errorPassEquals.equals("") ) {
            data[0]=login;
            data[1]=name;
            data[2]=password;
            data[3]=gender;
            data[4]=country;
            data[5]=aboutYourself;
            data[6]=newsletter;

            db.Writer(data);
            getServletContext().getRequestDispatcher("/WEB-INF/views/succes.jsp").forward(req, resp);

        }
        else{
            req.setAttribute("errorNameEquals",errorNameEquals );
            req.setAttribute("errorLogin", errorLogin );
            req.setAttribute("errorNameMatches", errorNameMatches);
            req.setAttribute("errorLoginMatches", errorLoginMatches );
            req.setAttribute("errorPassLength", errorPassLength);
            req.setAttribute("errorPassEquals", errorPassEquals);
            req.setAttribute("errorGender", errorGender);

            getServletContext().getRequestDispatcher("/WEB-INF/views/registration.jsp").forward(req, resp);
        }
    }

}