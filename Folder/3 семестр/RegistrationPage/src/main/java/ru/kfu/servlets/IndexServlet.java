package ru.kfu.servlets;

import org.dom4j.DocumentException;
import org.json.JSONException;
import ru.kfu.entites.Artist;
import ru.kfu.lastFM.API;
import ru.kfu.lastFM.Parser;
import ru.kfu.repositories.DataBase;
import ru.kfu.weather.Weather;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Reenaz on 30.09.2016.
 */
public class IndexServlet extends HttpServlet {
    public static DataBase db = new DataBase();
    public Weather weather = new Weather();

    protected  void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if((String) session.getAttribute("login") == null){
            getServletContext().getRequestDispatcher("/WEB-INF/views/index.jsp").forward(req, resp);
        }
        else{
            getServletContext().getRequestDispatcher("/WEB-INF/views/UserPage.jsp").forward(req, resp);

        }


    }

    protected void doPost(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        Parser parser = new Parser();
        API api = new API();

        if(db.checkUser(req.getParameter("loginUser"), req.getParameter("passwordUser"))){
            session.setAttribute("login", req.getParameter("loginUser"));
            session.setMaxInactiveInterval(100);
            req.setAttribute("weather", weather.getWeather());
            ArrayList<Artist> artists = new ArrayList<Artist>();
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("C://artists.txt")));
            int c;
            while ((c=reader.read()) != -1) {
                String nameArtist="";
                nameArtist += (char) c;
                while (((char)(c=reader.read()))!= ',' && c != -1) {
                    nameArtist += (char) c;
                }
                try {
                    artists.add(new Artist(
                            parser.getNameArtist(api.getSizeLoveSinger(nameArtist)),
                            parser.getSizeListener(api.getSizeLoveSinger(nameArtist)),
                            parser.getImgArtist(api.getSizeLoveSinger(nameArtist))
                    ));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                nameArtist="";
                reader.skip(1);
            }
            reader.close();
            req.setAttribute("artists", artists);

            getServletContext().getRequestDispatcher("/WEB-INF/views/UserPage.jsp").forward(req, resp);
        }
        else{
            resp.sendRedirect("/index");
        }
    }
}
