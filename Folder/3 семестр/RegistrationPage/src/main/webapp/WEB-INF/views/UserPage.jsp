<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: Reenaz
  Date: 30.09.2016
  Time: 14:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Page</title>
</head>
<body>
    <h1>Страница пользователя</h1>
    Погода в Казазни: <%= request.getAttribute("weather") %> C°
    <form action="/" method="post">
        <input type="submit" value="Log out">
    </form>
    <div>
        <c:choose>
            <c:when test="${fn:length(artists) gt 0}">
                <c:forEach items="${artists}" var="artist">
                    <p>listening to ${artist.name}: ${artist.sizeListener}</p>
                    <img src="<c:url value="${artist.img}"/>" alt="Картинки нет!">
                </c:forEach>
            </c:when>
        </c:choose>
    </div>


</body>
</html>
