import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Reenaz on 13.02.2017.
 */
public class Main {
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
        Scanner in = new Scanner(System.in);
        NodeList nodes;
        XPathExpression expr;
        XPath xpath;

        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(true); // never forget this!
        DocumentBuilder builder = domFactory.newDocumentBuilder();

        XPathFactory factory = XPathFactory.newInstance();

        try{
            org.w3c.dom.Document doc = builder.parse("1.xml");
            while (true){
                try{
                    xpath = factory.newXPath();
                    expr = xpath.compile(in.nextLine());
                    Object result = expr.evaluate(doc, XPathConstants.NODESET);
                    nodes = (NodeList) result;
                    for (int i = 0; i < nodes.getLength(); i++) {
                        System.out.println(nodes.item(i).getTextContent());
                    }
                }catch(XPathExpressionException e){
                    e.printStackTrace();
                    System.out.println("Ошибка в XPath выражении!\n");
                }

            }
        }catch(Exception e){
            System.out.println("Файл не найден");
        }


    }
}
