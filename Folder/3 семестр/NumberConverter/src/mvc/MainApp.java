package mvc;

import javax.swing.*;

/**
 * Created by Reenaz on 28.02.2017.
 */
public class MainApp {
        /**
         * Initializes and starts the Unit Converter application.
         */
        public static void main(String[] args)
        {
            // use look and feel for my system (Win32)
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            }
            catch (Exception e) {}

            ConvModel model = new ConvModel();
            ConvView view = new ConvView(model);

            view.setVisible(true);
        }

}
