import javax.swing.*;
import java.awt.*;

/**
 * Created by Reenaz on 22.02.2017.
 */
public class ConverterView extends JFrame {
    private JPanel mainPanel;
    private JTextField resultField;
    private JTextField KgTxtField;
    private JTextField MetrTxtField;
    private JButton convertBtn;

    public ConverterView(){
        super("Converter");

        super.setContentPane(mainPanel);
        super.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        super.setMinimumSize(new Dimension(800, 600));

        mainPanel = new JPanel();
        super.getContentPane().add(mainPanel);

        KgTxtField = new javax.swing.JTextField();
        MetrTxtField = new javax.swing.JTextField();
        resultField = new JTextField();
        resultField.setColumns(10);
        KgTxtField.setColumns(10);
        MetrTxtField.setColumns(10);
        mainPanel.add(resultField);
        mainPanel.add(KgTxtField);
        mainPanel.add(MetrTxtField);

        super.setVisible(true);

    }
}
