/**
 * Created by Reenaz on 22.02.2017.
 */
public class MetrToKmConverter extends AbstractConverter {
    @Override
    public int valueCOnvert(int value) {
        return value/1000;
    }
}
