package Model;

/**
 * Created by Reenaz on 25.11.2016.
 */
public class Contact {
    private int id;
    private String name;
    private String second_name;
    private int number;
    private Boolean favorite;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public String getSecond_name() { return second_name;}
    public void setSecond_name(String second_name) {this.second_name = second_name;}
    public Boolean getFavorite() { return favorite; }
    public void setFavorite(Boolean favorite) { this.favorite = favorite;}
}
