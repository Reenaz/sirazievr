import Model.Contact;

import javax.lang.model.element.Name;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window extends JFrame {

    Object[] headers = {"ID", "Name", "Second name", "Number", "Favorite"};
    Object[][] data = {
    };
    private JTable table;

    public Window(String name){
        super(name);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(100, 100, 600, 500);
        createGui();
        table = new JTable(data, headers);
        JScrollPane scrollPane = new JScrollPane(table);
        table.setPreferredScrollableViewportSize(new Dimension(350, 500));
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(scrollPane, BorderLayout.WEST);
        setResizable(false);
        setVisible(true);
    }

    private void createGui() {
        Container panel = getContentPane();
        panel.setLayout(new BorderLayout());
        JPanel panel1 = new JPanel();
        panel1.setLayout(new FlowLayout());
        panel1.setBounds(360, 0, 150, 500);
        JTextField ID;
        JTextField Name;
        JTextField Second_name;
        JTextField Number;

        panel1.add(ID = new JTextField("ID", 13));
        panel1.add(Name =  new JTextField("Name", 13));
        panel1.add(Second_name =  new JTextField("Second name", 13));
        panel1.add(Number = new JTextField("Number", 13));

        JCheckBox checkBox = new JCheckBox();
        JLabel label = new JLabel("Is it a favorite contact?");
        panel1.add(label);
        panel1.add(checkBox);
        JButton button = new JButton("Add contact");
        panel1.add(button);
        panel.add(panel1);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Contact contact=new Contact();
                contact.setName(Name.getText());
                contact.setId(Integer.valueOf(ID.getText()));
                contact.setSecond_name(Second_name.getText());
                contact.setNumber(Integer.valueOf(Number.getText()));

            }
        });
    }
}
