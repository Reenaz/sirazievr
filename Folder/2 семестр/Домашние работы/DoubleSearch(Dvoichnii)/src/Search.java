import java.io.BufferedReader;  //двоичный поиск
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
public class Search {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Random random = new Random();
        System.out.println("Введите размер массива");
        int a=Integer.parseInt(reader.readLine());
        int[] arr = new int[a];
        find(arr);
    }

    public static void find(int[] arr) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Random random = new Random();
        System.out.println("Введите число которое надо найти");
        int b=Integer.parseInt(reader.readLine());
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < arr.length; i++) {
            arr[i] =i+1;/* random.nextInt(100);*/
        }
       /* arr = Sort(arr);*/
        int middle = arr.length / 2;
        int start=0;
        int finish=arr.length;
            int count = 0;
            while (middle != 0 && start != arr.length-1 && count == 0) {
                 if (arr[middle] == b) {
                    System.out.println("Element naiden, on nahoditsya na " + (middle+1) + " pozicii");
                    count++;
                } else if (arr[middle] > b) {
                     finish=middle;
                    middle =(middle+start)/ 2;
                }
                else if (arr[middle] < b) {
                     start=middle;
                    middle =finish-(finish-middle)/2;
                }
            }
        if (count==0) System.out.println("Element ne naiden");
        long time = System.currentTimeMillis() - startTime;
        System.out.println("Programa vipolnyalas' "+time+" millisekund");
        }


    public static int[] Sort(int[] arr) {
        int b;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - i-1; j++) {
                if (arr[j] > arr[j + 1]) {
                    b = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = b;
                }
            }
        }
        return arr;
    }
}
