public interface Map<K extends  Comparable,V> {
    void add(K key, V value);
     V get (K key);
    void remove(K key);
}
