class TreeMap<K extends Comparable, V> implements Map<K, V> {
    private K key;
    private V value;
    private TreeMap<K, V> rightSibling;
    private TreeMap<K, V> leftSibling;
    int rh=0;
    int lh=0;
    public TreeMap() {

    }


    public void leftTurn(){
        TreeMap<K,V> oldroot = root;
        TreeMap<K,V> node =root.rightSibling;
        oldroot.rightSibling = node.leftSibling;
        root=node;
        root.leftSibling=oldroot;
    }
    public void rightTurn(){
        TreeMap<K,V> oldroot = root;
        TreeMap<K,V> node =root.leftSibling;
        oldroot.leftSibling = node.rightSibling;
        root=node;
        root.rightSibling=oldroot;
    }

    public TreeMap(K t, V value) {
        this.key = t;
        this.value = value;
    }

    private TreeMap<K, V> root = null;

    private int getHigh(){
        rh++;
        int rh = this.rightSibling == null?0: this.rightSibling.getHigh();
        lh++;
        int lh  = this.leftSibling == null ? 0:this.leftSibling.getHigh();

        return rh=(rh>lh)?rh:lh ;
    }

    public void add(K k, V v) {
        TreeMap<K, V> x = root, y = null;
        while (x != null) {
            int cmp = k.compareTo(x.key);
            if (cmp == 0) {
                x.value = v;
                return;
            } else {
                y = x;
                if (cmp < 0) {
                    x = x.leftSibling;
                } else x = x.rightSibling;
            }
        }
        TreeMap<K, V> newNode = new TreeMap<K, V>(k, v);
        if (y == null) {
            root = newNode;
        } else {
            if (k.compareTo(y.key) < 0) {
                y.leftSibling = newNode;
            } else y.rightSibling = newNode;
        }
    }


    public void remove(K k) {
        TreeMap<K, V> x = root;
        TreeMap<K, V> y = null;
        while (x != null) {
            int cmp = k.compareTo(x.key);
            if (cmp == 0) {
                break;
            } else {
                y = x;
                if (cmp < 0) {
                    x = x.leftSibling;
                } else x = x.rightSibling;
            }
        }
        if (x == null) {
            return;
        }
        if (x.rightSibling == null) {
            if (y == null) {
                root = x.leftSibling;
            } else if (x == y.leftSibling) {
                y.leftSibling = x.leftSibling;
            } else y.rightSibling = x.leftSibling;
        } else {
            TreeMap<K, V> leftMin = x.rightSibling;
            y = null;
            while (leftMin.leftSibling != null) {
                y = leftMin;
                leftMin = leftMin.leftSibling;
            }
            if (y != null) {
                y.leftSibling = leftMin.rightSibling;
            } else {
                x.rightSibling = leftMin.rightSibling;
            }
            x.key = leftMin.key;
            x.value = leftMin.value;
        }
    }

    public V get(K key) {
        TreeMap<K, V> x = root;
        while (x != null) {
            int cmp = key.compareTo(x.key);
            if (cmp == 0) return x.value;
            if (cmp < 0) {
                x = x.leftSibling;
            } else x = x.rightSibling;
        }
        return null;
    }



