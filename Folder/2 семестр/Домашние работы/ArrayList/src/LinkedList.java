public class LinkedList implements List{
    long start =System.nanoTime();
    private Link head=null;
    private Link lastLink=null;
    private int size=0;
    @Override
    public void add(int el) {
        size++;
        if (head ==null){
            head = new Link(el);
            lastLink = head;
            return;
        }
        Link newLink = new Link(el);
        lastLink.setNext(newLink);
        lastLink=newLink;

    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int getAt(int index) {
        if(index >= size || index<0)
            throw new ArrayIndexOutOfBoundsException();
        Link l=head;
        for (int i=0;i<index; i++){
            l=l.getNext();
        }
        return l.getValue();
    }

    @Override
    public void remove(int index) {
        if (index>=size||index<0)
            throw new ArrayIndexOutOfBoundsException();
        Link l=head;
        for (int i=0;i<index-1; i++){
            l=l.getNext();
        }
        Link tolink=l.getNext().getNext();
        l.setNext(tolink);
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void set(int index, int el) {
        if (index>=size||index<0)
            throw new ArrayIndexOutOfBoundsException();
        Link l=head;
        for (int i=0; i<=index; i++){
            l=l.getNext();
        }
        l.setValue(el);
    }

    @Override
    public void clear() {
        head = null;
    }

    public void Time (){
        long end =System.nanoTime();
        long result=end-start;
        System.out.println(result);
    }
}
class Link{
    private int value;
    public Link(int value){
        this.value=value;
    }

    public int getValue() {
        return value;
    }

    private Link next;
    public void setNext(Link l){
        next=l;
    }
    public Link getNext(){
        return next;
    }

    public void setValue(int el) {
        this.value=el;
    }
}
