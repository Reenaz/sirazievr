public interface List {
   void add(int el);
   boolean isEmpty();
   int getAt(int index);
   void remove(int index);
   int getSize();
   void set(int index, int el);
   void  clear();

   void Time();
}