public class ArrayList implements List {
    long start =System.nanoTime();
   private int[] elementsArray = new int[10];
    private int currentIndex = 0;


    private void scaleArray() {
        int length = this.elementsArray.length;
        int newLength = 2*length;
        int [] newArr = new int[newLength];
        for (int i=0; i<length; i++) {
            newArr[i] = this.elementsArray[i];
        }
        this.elementsArray = newArr;
    }


    public void add(int el) {
        this.elementsArray[currentIndex] = el;
        currentIndex++;

        if (currentIndex==this.elementsArray.length){
            this.scaleArray();
        }
    }
   public boolean isEmpty() {
        return currentIndex==0;
    }

   public int getAt(int index) {
       if (index>=currentIndex) {
           throw new ArrayIndexOutOfBoundsException();
       }

       return this.elementsArray[index];

    }

  public void remove(int index) {
        this.elementsArray[index]=0;
      for (int i = index; i < currentIndex; i++) {
          this.elementsArray[i] = this.elementsArray[i+1];
          deleteNull();
      }
      currentIndex--;
    }

    public int getSize() {
       return currentIndex;
   }

    public void set(int index, int el) {
        if (index>=currentIndex) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }
    public void clear(){
        for (int i=0; i<currentIndex; i++){
            this.elementsArray[i]=0;
            deleteNull();
        }
        currentIndex=0;
    }

    public void deleteNull(){
        int count;
        if (elementsArray.length-currentIndex<70/100*elementsArray.length){

            int[] newArr = new int[elementsArray.length-currentIndex];
            for (int i = 0; i < elementsArray.length - currentIndex; i++) {
                newArr[i] = this.elementsArray[i];
            }
            this.elementsArray = newArr;

        }
    }
    public void Time (){
        long end =System.nanoTime();
        long result=end-start;
        System.out.println(result);
    }

}
