import java.io.*;
public class Main {
    public static void main(String[] args) {
        OutputStream stream = null;
        try {
            stream = new FileOutputStream("C:/test/test.txt");
            stream.write("La la la".getBytes());
            int n=1000000;
            double start =System.currentTimeMillis();
            for (int i=0; i<=n; i++){
                stream.write("reee".getBytes());
            }
            double finish=System.currentTimeMillis()-start;
            System.out.println(finish/1000);
        } catch (FileNotFoundException exception) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Ups");
        }
        try {
            stream.close();
        } catch (FileNotFoundException exception) {
            System.out.println("File not found");
        } catch (IOException exception) {
            System.out.println("Ups");
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                }catch(IOException e){

                }
            }
        }
    }
}
