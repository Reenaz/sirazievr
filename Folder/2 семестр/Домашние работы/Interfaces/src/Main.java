public class Main {
    public static void main(String[] args) {
        Moveable[] s = new Moveable[4];
        s[0]=new Dog();
        s[1]= new Cat();
        s[2]=new Robot();
        s[3]=new Bird();
        for (int i=0; i<4;i++){
            s[i].move();
        }
    }
}
