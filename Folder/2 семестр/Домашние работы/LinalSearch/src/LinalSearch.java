import java.io.BufferedReader;                      //линейный поиск
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
public class LinalSearch {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите размер массива");
        int a=Integer.parseInt(reader.readLine());
        int[] arr = new int[a];
        find(arr);

    }

    public static void find(int[] arr) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Random random = new Random();
        System.out.println("Введите число которое надо найти");
        int count=0;

        int b = Integer.parseInt(reader.readLine());
        long start = System.currentTimeMillis();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i + 1;
        }
       /* arr = Sort(arr);*/
        for (int i = 0; i < arr.length; i++) {
            if (count!=0) break;
            if (arr[i]==b){
                count++;
                System.out.println("Element naiden, on nahoditsya na " + (i+1) + " pozicii");
            }
        }
        if (count==0)
            System.out.println("Element ne naiden");
        long time = System.currentTimeMillis()-start;
        System.out.println("Programa vipolnyalas' "+time+" millisekund");
        }

    public static int[] Sort(int[] arr) {
        int b;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - i-1; j++) {
                if (arr[j] > arr[j + 1]) {
                    b = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = b;
                }
            }
        }
        return arr;
    }
}

