public class MyStack<T> implements Stack<T> {
    private int currentIndex=0;
    private int up=0;
    private  T[] data= (T[]) new Object[5];
    @Override
    public void push(T element) {
    data[currentIndex]=element;
        currentIndex++;
        if (currentIndex==this.data.length-1){
            scaleArray();
        }
    }

    @Override
    public T pop() {
        if(currentIndex==0){
            return null;
        }
        T head=data[currentIndex-1];

        currentIndex--;
        if(data.length-currentIndex>20){
            T [] newArr = (T[])new Object[this.data.length-10];
            for (int i=0; i <=currentIndex; i++){
                newArr[i]=data[i];
                this.data = newArr;
            }
        }
        System.out.println(head);
        return head;
    }

    @Override
    public T peek() {
        if(currentIndex==0){
            return null;
        }
        T head=data[currentIndex-1];
        System.out.println(head);
        return head;
    }

    @Override
    public Boolean isEmpty() {
        System.out.println(data[currentIndex]==null);
        return data[currentIndex]==null;
    }

    private void scaleArray() {
        int length = this.data.length;
        int newLength = 2*length;
        T [] newArr = (T[])new Object[newLength];
        for (int i=0; i<length; i++) {
            newArr[i] = this.data[i];
        }
        this.data = newArr;
    }
}
