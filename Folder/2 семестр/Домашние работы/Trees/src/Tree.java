public class Tree<T extends Comparable> {
    private TreeNode<T> root;

    public void addElement(T t) {
        if (root == null) {
            root = new TreeNode<T>(t);
        } else {
            root.addElement(t);
        }
    }

    public boolean find(T t) {
        return root.find(t);
    }

    public boolean get(T key) {
        if (root == null) {
            return false;
        } else {
            return root.find(key);
        }
    }
}

class TreeNode<T extends Comparable> {
    private T value;
    private TreeNode<T> rightSibling;
    private TreeNode<T> leftSibling;

    public TreeNode(T t) {
        this.value = t;
    }

    public boolean find(T key) {
        int cVal = this.value.compareTo(key);
        switch (cVal) {
            case 0:
                return true;

            case 1:
                return rightSibling != null && rightSibling.find(key);
            case -1:
                return leftSibling != null && leftSibling.find(key);
            default:
                return false;

        }
    }

    public void addElement(T t) {
        int cVal = this.value.compareTo(t);

        if (cVal == 1) {
            if (rightSibling == null) {
                rightSibling = new TreeNode<T>(t);
            } else rightSibling.addElement(t);
        } else if (cVal == -1) {
            if (leftSibling == null) {
                leftSibling = new TreeNode<T>(t);
            } else leftSibling.addElement(t);
        }
    }

}
