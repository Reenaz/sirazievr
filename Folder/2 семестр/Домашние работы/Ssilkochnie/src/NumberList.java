import java.util.LinkedList;
import  java.util.Arrays;

public class NumberList <T extends Number>extends LinkedList<T>{

    public Double Sum(double []arr) {
        double sum=0;
        for (int i=0;i<this.size();i++){
            sum+=arr[i];
        }
        return sum;
    }
    public Integer Med(int [] arr){
        Arrays.sort(arr);
        int length=this.size();
        if(length%2==0){
            return (arr[length/2]+arr[length/2-1])/2;
        }
        else return arr[length/2];
    }
    public Double Mean(double []arr){
        double sum=Sum(arr);
        return Double.valueOf(sum/this.size());
    }
}