public class LinkedList<T> implements List<T>{
    private Link<T> head=null;
    private Link<T> lastLink=null;
    private int size=0;
    @Override
    public void add(int el) {
        size++;
        if (head ==null){
            head = new Link<T>(el);
            lastLink = head;
            return;
        }
        Link<T> newLink = new Link<T>(el);
        lastLink.setNext(newLink);
        lastLink=newLink;

    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public T getAt(int index) {
        if(index >= size || index<0)
            throw new ArrayIndexOutOfBoundsException();
        Link l=head;
        for (int i=0;i<index; i++){
            l=l.getNext();
        }
        return l.getValue();
    }

    @Override
    public void remove(int index) {
        if (index>=size||index<0)
            throw new ArrayIndexOutOfBoundsException();
        Link l=head;
        for (int i=0;i<index-1; i++){
            l=l.getNext();
        }
        Link tolink=l.getNext().getNext();
        l.setNext(tolink);
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void set(int index, T el) {
        if (index>=size||index<0)
            throw new ArrayIndexOutOfBoundsException();
        Link l=head;
        for (int i=0; i<=index; i++){
            l=l.getNext();
        }
        l.setValue(el);
    }

    @Override
    public void clear() {
        head = null;
    }
}
class Link{
    private T value;
    public Link(T value){
        this.value=value;
    }

    public int getValue() {
        return value;
    }

    private Link<T> next;
    public void setNext(Link l){
        next=l;
    }
    public Link<T> getNext(){
        return next;
    }

}
