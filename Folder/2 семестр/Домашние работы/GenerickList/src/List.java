public interface List<T> {
    void add(T el);
    boolean isEmpty();
    T getAt(int index);
    void remove(int index);
    int getSize();
    void set(int index, T el);
    void  clear();
}