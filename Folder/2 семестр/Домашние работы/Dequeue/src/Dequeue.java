public interface Dequeue<T> {
    void addFirst(T element);
    void addLast(T element);
    T pollFirst();
    T pollLast();
    T peekFirst();
    T peekLast();
    int sizeLast();
    int sizeFirst();
}
