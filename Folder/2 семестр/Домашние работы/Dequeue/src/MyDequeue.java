public class MyDequeue<T> implements  Dequeue<T> {
    private  T[] data= (T[]) new Object[30];
    private int currentIndex=data.length/2-1;
    private int down=currentIndex;
    @Override
    public void addFirst(T element) {
        data[currentIndex]=element;
        currentIndex++;
        if (currentIndex==data.length-1){
            this.scaleArray();
        }
    }

    @Override
    public void addLast(T element) {
        data[down]=element;
        down--;
        if (down==-1){
            this.scaleArray();
        }
    }

    @Override
    public T pollFirst() {
        T head=data[currentIndex-1];
        currentIndex--;

        System.out.println(head);
        return head;

    }

    @Override
    public T pollLast() {
        T head=data[down+1];
        down++;

        System.out.println(head);
        return head;
    }

    @Override
    public T peekFirst() {
        T head=data[currentIndex-1];
        System.out.println(head);
        return head;
    }

    @Override
    public T peekLast() {
        T head=data[down+1];
        System.out.println(head);
        return head;
    }

    @Override
    public int sizeLast() {
        System.out.println(data[down]);
        return currentIndex-down;
    }

    @Override
    public int sizeFirst() {
        System.out.println(data[currentIndex]);
        return currentIndex-down;
    }
    private void scaleArray() {
        int length = this.data.length;
        int newLength = length+20;
        T [] newArr = (T[])new Object[newLength];
        for (int i=down; i<=currentIndex; i++) {
            newArr[i+10] = this.data[i];
        }
        this.data = newArr;
    }
}
