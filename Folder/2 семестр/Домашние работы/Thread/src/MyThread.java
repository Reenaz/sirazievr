public class MyThread implements Runnable{
    public void run() {
        int i=0;
        while(true){
            System.out.println(" My thread"+ i++);
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (i ==10){
                System.out.println("Loading");
                try {
                    synchronized (Main.waitObject){
                        Main.waitObject.wait();
                    }
                    System.out.println("MyThread is wait ");
                    Main.waitObject.wait();
                } catch (Exception e) {
                    System.out.println("Sorry");
                }
            }
        }
    }
}
