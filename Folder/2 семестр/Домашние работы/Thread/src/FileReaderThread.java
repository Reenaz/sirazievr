import java.io.*;
import java.nio.charset.StandardCharsets;


class FileReaderThread extends Thread{
    private String data;
    private String fileName;
    private double offset;
    private double pieceLength;
    public FileReaderThread(String fileName, double offset, double length){
        this.fileName=fileName;
        this.offset=offset;
        this.pieceLength=length;
    }
    public String getData() {
        return this.data;
    }
    public void run(){
        File file = new File(fileName);
        long fileLength=file.length();
        long byteOffset= (long) (offset*fileLength);
        int piece = (int) (pieceLength*fileLength);
        try {
            InputStream stream = new FileInputStream(file);
            byte[] bytes = new byte[piece];

            stream.skip(byteOffset);
            stream.read(bytes);
            this.data = new String(bytes, StandardCharsets.UTF_8);
            stream.close();
        } catch (FileNotFoundException e) {
            System.out.println("Not File");

        }   catch (IOException e){
            System.out.println("This is a problem wth reading file");
        }
    }


}
