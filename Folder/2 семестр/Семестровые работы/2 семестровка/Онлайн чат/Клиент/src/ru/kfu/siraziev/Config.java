package ru.kfu.siraziev;

public class Config {
    private static int PORT = 5000;
    private static int HISTORY_LENGTH = 50 ;
    private static String HELLO_MESSAGE = "User join to the chat(Auto-message)";

    public static int getPORT(){ return PORT; };
    public static int getHISTORY_LENGTH(){ return  HISTORY_LENGTH; }
    public static String getHELLO_MESSAGE(){ return HELLO_MESSAGE; }

}
