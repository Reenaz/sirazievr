package ru.kfu.siraziev;

import ru.kfu.siraziev.controler.ChatControler;
import ru.kfu.siraziev.controler.ChatControlerInterface;
import ru.kfu.siraziev.model.ChatModel;
import ru.kfu.siraziev.model.ChatModelInterface;

/**
 * Created by Reenaz on 29.05.2016.
 */
public class ChatMain {
    public static void main(String[] args){
        try {
            ChatModelInterface model = new ChatModel();
            ChatControlerInterface controler = new ChatControler(model);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
