package ru.kfu.siraziev.view;

import ru.kfu.siraziev.controler.ChatControlerInterface;
import ru.kfu.siraziev.model.ChatModelInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Reenaz on 29.05.2016.
 */
public class ChatView implements ChatObserver, ActionListener{
    ChatControlerInterface controler;
    ChatModelInterface model;

    JFrame frame;
    JButton button;
    JTextArea inputArea;
    JTextArea outputArea;
    JTextField ipField;

    public ChatView(ChatControlerInterface controler, ChatModelInterface model) {
        this.controler = controler;
        this.model = model;
        model.registerObserver((ChatObserver) this);
    }

    public void createView() {
        frame = new JFrame("Online chat");
        frame.setLocationRelativeTo(null);
        frame.setSize(500, 500);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        JPanel startPanel = new JPanel();
        JPanel btnPanel = new JPanel();

        startPanel.setLayout(new FlowLayout());
        panel.setLayout(new BorderLayout());
        JLabel ipLabel = new JLabel("IP");

        button = new JButton("Подключиться");
        button.addActionListener(this);

        inputArea = new JTextArea(5, 25);
        outputArea = new JTextArea("Введите ваш логин\n", 20, 30);
        ipField = new JTextField(12);

        inputArea.setLineWrap(true);
        outputArea.setLineWrap(true);
        outputArea.setEditable(false);

        btnPanel.add(button);
        JScrollPane scrollInputArea = new JScrollPane(inputArea);
        JScrollPane scrollOutputArea = new JScrollPane(outputArea);
        startPanel.add(ipLabel, BorderLayout.NORTH);
        startPanel.add(ipField, BorderLayout.CENTER);
        startPanel.add(btnPanel, BorderLayout.SOUTH);

        panel.add(scrollOutputArea, BorderLayout.NORTH);
        panel.add(startPanel, BorderLayout.CENTER);
        panel.add(scrollInputArea, BorderLayout.WEST);

        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    public JButton getButton(){ return button; }

    public JTextArea getOutputArea() {
        return outputArea;
    }

    public JTextArea getInputArea() {
        return inputArea;
    }

    public JTextField getIpField() {
        return ipField;
    }

    public void clearArea(){
        getInputArea().setText(null);
    }

    public void changeBtnValue(){ getButton().setText("Отправить");}

    public boolean checkArea(){
        if(getInputArea().getText().isEmpty()){
            return false;
        }
        else{
            StringBuilder text = new StringBuilder(getInputArea().getText());
            for(int i = 0;i<text.length();i++){
                if(text.charAt(i) != ' '){
                    return true;
                }
            }
            return false;
        }

    }


    @Override
    public void updateChat() {
        getOutputArea().append("["+model.getMessage().getLogin()+"]: "+" "+model.getMessage().getMessage()+"\n");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if( button.getText().equals("Подключиться")){
            if(checkArea()){
                controler.startChat( getIpField().getText(), getInputArea().getText());
                clearArea();
                getIpField().setEditable(false);
            }
            else{
                getOutputArea().append("Ошибка! Пустой логин\n");
            }
        }
        else{
            controler.sendMessage(getInputArea().getText());
            clearArea();
        }

    }
}
