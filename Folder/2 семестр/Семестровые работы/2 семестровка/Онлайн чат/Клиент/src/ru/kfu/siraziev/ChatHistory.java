package ru.kfu.siraziev;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ChatHistory implements Serializable {
    private List<Message> history;


    public ChatHistory() {
        this.history = new ArrayList<Message>(Config.getHISTORY_LENGTH());

    }
    public synchronized void  writeMessage(String message){
        PrintStream out = null;
        try {

            out = new PrintStream(

                    new BufferedOutputStream(

                            new FileOutputStream("messages.txt", true)));

            out.println(message);


        } catch(IOException e) {

            e.printStackTrace();

        } finally {

            if (out != null) {
                out.close();
            }
        }
    }

    public void addMessage(Message message){
        if (this.history.size() > Config.getHISTORY_LENGTH()){
            this.history.remove(0);
        }

        this.history.add(message);
    }

    public List<Message> getHistory(){
        return this.history;
    }

}
