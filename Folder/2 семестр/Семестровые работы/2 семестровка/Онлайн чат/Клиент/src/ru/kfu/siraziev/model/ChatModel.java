package ru.kfu.siraziev.model;

import ru.kfu.siraziev.ChatHistory;
import ru.kfu.siraziev.Message;
import ru.kfu.siraziev.User;
import ru.kfu.siraziev.view.ChatObserver;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by Reenaz on 29.05.2016.
 */
public class ChatModel implements ChatModelInterface, Runnable {
    private final int serverPort = 5000;

    ArrayList ChatObservers = new ArrayList();
    private String ipAdress ;
    private String login ;
    private Socket socket;
    private ObjectInputStream input;
    private ObjectOutputStream output;
    private User user;
    private Message msg;
    private ChatHistory history;

    public void sendMessage(String message){
        try {
            if(message.equals(null)) {
                msg = new Message("ОШИБКА", "Пустое сообщение!");
                notifyObservers();
            }
            else {
                output.writeObject(new Message(user.getLogin(), message));

            }
        }
        catch(Exception e){
            msg = new Message("ОШИБКА", "Ошибка при отправке сообщения!");
            notifyObservers();
        }
    }

    @Override
    public Message getMessage() {
        return msg;
    }

    @Override
    public void setValues(String ip, String login) {
        this.ipAdress = ip;
        this.login = login;
    }

    @Override
    public void registerObserver(ChatObserver o) {
        ChatObservers.add(o);
    }

    @Override
    public void removeObserver(ChatObserver o) {
        int i = ChatObservers.indexOf(o);
        if(i>=0){
            ChatObservers.remove(o);
        }

    }

    @Override
    public void notifyObservers() {
        for( int i = 0; i<ChatObservers.size(); i++){
            ChatObserver o = (ChatObserver) ChatObservers.get(i);
            o.updateChat();
        }
    }



    @Override
    public void run() {
        try {
            socket = new Socket(ipAdress, serverPort);

            output = new ObjectOutputStream(socket.getOutputStream());
            input = new ObjectInputStream(socket.getInputStream());

            user = new User(login);

            msg = new Message(user.getLogin(), "User join to the chat(Auto-message)");// ждем пока пользователь введет что-то и нажмет кнопку Enter.
            output.writeObject(msg);

            this.history = (ChatHistory) input.readObject();
            for(Message x: history.getHistory()){
                msg = x;
                notifyObservers();
            }

            while (true) {
                this.msg = (Message) input.readObject();
                notifyObservers();            }
        }
        catch (Exception e){
            msg = new Message("ОШИБКА", "Соединение с сервером потеряно!");
            notifyObservers();
            try{socket.close();}
            catch(Exception c){}
        }
    }
}
