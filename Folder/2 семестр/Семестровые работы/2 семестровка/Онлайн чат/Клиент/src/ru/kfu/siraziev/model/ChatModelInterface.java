package ru.kfu.siraziev.model;

import ru.kfu.siraziev.Message;
import ru.kfu.siraziev.view.ChatObserver;

/**
 * Created by Reenaz on 29.05.2016.
 */
public interface ChatModelInterface extends Runnable{
    void registerObserver(ChatObserver o);
    void removeObserver(ChatObserver o);
    void notifyObservers();


    void sendMessage(String message);
    Message getMessage();
    void setValues(String ip, String login);
}
