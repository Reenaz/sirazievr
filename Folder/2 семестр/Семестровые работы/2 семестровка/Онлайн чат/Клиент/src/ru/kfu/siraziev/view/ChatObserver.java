package ru.kfu.siraziev.view;

/**
 * Created by Reenaz on 29.05.2016.
 */
public interface ChatObserver {
    void updateChat();
}
