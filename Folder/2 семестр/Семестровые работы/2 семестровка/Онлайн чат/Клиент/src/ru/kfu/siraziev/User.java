package ru.kfu.siraziev;

/**
 * Created by Reenaz on 29.05.2016.
 */
public class User {
    public String login = null;
    public User(String login){
        this.login = login;
    }

    public String getLogin(){
        return login;
    }

    public void delLogin(){
        this.login = null;
    }
}
