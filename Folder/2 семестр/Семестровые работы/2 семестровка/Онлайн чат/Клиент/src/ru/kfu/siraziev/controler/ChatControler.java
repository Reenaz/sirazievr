package ru.kfu.siraziev.controler;

import ru.kfu.siraziev.model.ChatModelInterface;
import ru.kfu.siraziev.view.ChatView;

/**
 * Created by Reenaz on 29.05.2016.
 */
public class ChatControler implements ChatControlerInterface {
    ChatModelInterface model;
    ChatView view;

    public ChatControler(ChatModelInterface model){
        this.model = model;
        this.view = new ChatView(this, model);
        this.view.createView();
    }

    @Override
    public void startChat(String ipAdress, String login) {
        view.changeBtnValue();
        model.setValues(ipAdress, login);
        Thread t = new Thread(model);
        t.start();
    }

    @Override
    public void sendMessage(String message) {
        model.sendMessage(message);
    }


}
