package ru.kfu.siraziev;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

import static ru.kfu.siraziev.Server.getChatHistory;
import static ru.kfu.siraziev.Server.getUserList;

public class ClientThread extends Thread {
    private Socket socket;
    private Message c;
    private String login;
    public ObjectInputStream inputStream;
    public ObjectOutputStream outputStream;
    private  String message;


    public ClientThread(Socket socket) {
        this.socket = socket;
        this.start();
    }

    public void run() {
        try {
            startChat(this);

            while (true) {
                this.c = (Message) inputStream.readObject();
                System.out.println("[" + login + "]: " + c.getMessage());
                message = "["+login+"]: "+ c.getMessage()+"\n";
                getChatHistory().writeMessage(message);


                getChatHistory().addMessage(this.c);
                this.c.setOnlineUsers(getUserList().getUsers());

                if (!c.getMessage().equals(Config.getHELLO_MESSAGE())) {
                    System.out.println("Рассылаем сообщение: " + c.getMessage() + "");
                    this.broadcast(getUserList().getClientsList(), this.c);
                }
            }

        } catch (SocketException e) {
            System.out.println(login + " отключился!");
            getUserList().deleteUser(login);
            broadcast(getUserList().getClientsList(), new Message("СЕРВЕР-БОТ", "Пользователь " + login + " отключился от сервера", getUserList().getUsers()));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void startChat(ClientThread thread){
        try {
            inputStream = new ObjectInputStream(thread.socket.getInputStream());
            outputStream = new ObjectOutputStream(thread.socket.getOutputStream());

            thread.c = (Message) inputStream.readObject();
            thread.login = thread.c.getLogin();

            outputStream.writeObject(getChatHistory());
            thread.broadcast(getUserList().getClientsList(), new Message("СЕРВЕР-БОТ", "The user " + login + " has been connect"));

            getUserList().addUser(login, socket, outputStream, inputStream);



            thread.c.setOnlineUsers(getUserList().getUsers());
            thread.broadcast(getUserList().getClientsList(), this.c);
        }catch(Exception e){
            System.out.println("Ошибка при подключении нового пользователя");
            e.printStackTrace();
        }
    }

    private void broadcast(ArrayList<ru.kfu.siraziev.Client> clientsArrayList, Message message) {
        try {
            for (ru.kfu.siraziev.Client client : clientsArrayList) {

                    client.getThisObjectOutputStream().writeObject(message);

            }
        } catch (SocketException e) {
            System.out.println("in broadcast: " + login + " disconnected!");
            getUserList().deleteUser(login);
            this.broadcast(getUserList().getClientsList(), new Message("Server-Bot", "The user " + login + " has been disconnected", getUserList().getUsers()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}