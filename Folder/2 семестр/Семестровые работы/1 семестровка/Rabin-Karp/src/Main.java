import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Reenaz on 18.04.2016.
 */
public class Main {
    public static void main(String[] args) throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Алгоритм Рабина-Карпа\n");
        System.out.println("\nВведите текст\n");
        String text = br.readLine();
        System.out.println("\nВведите подстроку\n");
        String pattern = br.readLine();
        System.out.println("\nРезультат : \n");
        RabinKarp rk = new RabinKarp(text, pattern);
    }
}
