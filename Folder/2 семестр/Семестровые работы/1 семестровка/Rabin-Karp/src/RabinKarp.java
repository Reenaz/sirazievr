import java.math.BigInteger;
import java.util.Random;

public class RabinKarp
{
    /** шаблон **/
    private String pat;
    /** значение хэша шаблона **/
    private long patHash;
    /** длина шаблона **/
    private int M;
    /** какое-то большое число **/
    private long Q;
    /** основание системы счисления **/
    private int R;
    /** формула R^(M-1) % Q используемая при хешировании **/
    private long RM;

    /** Конструктор **/
    public RabinKarp(String txt, String pat)
    {
        this.pat = pat;
        R = 256;
        M = pat.length();
        Q = longRandomPrime();
        /** вычисление R^(M-1) % Q для использования при удалении главной цифры **/
        RM = 1;
        for (int i = 1; i <= M-1; i++)
            RM = (R * RM) % Q;
        patHash = hash(pat, M);
        int pos = search(txt);
        if (pos == -1)
            System.out.println("\nСтрока не найдена\n");
        else
            System.out.println("Строка найдена на позиции : "+ pos);
    }
    /** Вычисление хеша **/
    private long hash(String key, int M)
    {
        long h = 0;
        for (int j = 0; j < M; j++)
            h = (R * h + key.charAt(j)) % Q;
        return h;
    }
    /** посимвольная проверка**/
    private boolean check(String txt, int i)
    {
        for (int j = 0; j < M; j++)
            if (pat.charAt(j) != txt.charAt(i + j))
                return false;
        return true;
    }
    /** проверка точного соответствия**/
    private int search(String txt)
    {
        int N = txt.length();
        if (N < M) return N;
        long txtHash = hash(txt, M);
        /** проверка соответствия в начале **/
        if ((patHash == txtHash) && check(txt, 0))
            return 0;
        /** проверка на хеши, если они совпадают проверка на точное соотвествие**/
        for (int i = M; i < N; i++)
        {
            txtHash = (txtHash + Q - RM * txt.charAt(i - M) % Q) % Q;
            txtHash = (txtHash * R + txt.charAt(i)) % Q;
            // найдено
            int offset = i - M + 1;
            if ((patHash == txtHash) && check(txt, offset))
                return offset;
        }
        /** не найдено **/
        return -1;
    }
    /** генерируем очень большое число **/
    private static long longRandomPrime()
    {
        BigInteger prime = BigInteger.probablePrime(31, new Random());
        return prime.longValue();
    }
}