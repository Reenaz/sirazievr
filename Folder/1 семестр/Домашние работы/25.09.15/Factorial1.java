public class Factorial1{
	public static void main(String[] args){
		java.util.Scanner s = new java.util.Scanner(System.in); 
	    int x = s.nextInt(); 
		int f=1;
		if ((x==0) || (x==1)){
			f=1;
		}
		else{
			for(int d=2;d<=x;d++){
				f=f*d;
			}
			
		}
		System.out.println(f);
	}
}