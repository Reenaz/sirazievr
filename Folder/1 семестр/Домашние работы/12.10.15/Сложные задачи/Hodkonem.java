import java.util.Scanner;
public class Hodkonem {
    private static final int gor_razmer = 8;
    private static final int ver_razmer = 8;
    private static int gor[] = { 2, 1, -1, -2, -2, -1, 1, 2 };
    private static int ver[] = { -1, -2, -2, -1, 1, 2, 2, 1 };
    private static int acces[][] = { { 2, 3, 4, 4, 4, 4, 3, 2 }, { 3, 4, 6, 6, 6, 6, 4, 3 }, { 4, 6, 8, 8, 8, 8, 6, 4 }, { 4, 6, 8, 8, 8, 8, 6, 4 }, { 4, 6, 8, 8, 8, 8, 6, 4 }, { 4, 6, 8, 8, 8, 8, 6, 4 }, { 3, 4, 6, 6, 6, 6, 4, 3 },
            { 2, 3, 4, 4, 4, 4, 3, 2 } };
    private static int koordinata1, koordinata2, variant_hoda, counter = 0;
    private static int board[][] = new int[gor_razmer][ver_razmer];
        public Hodkonem() {
        
    }    
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("������� �������������� ���������� ��(0 - 7): ");
        while (!s.hasNextInt())
            s.next();
        koordinata1 = s.nextInt();
        System.out.print("������� ������������ ���������� ��(0 - 7): ");
        while (!s.hasNextInt())
            s.next();
        koordinata2 = s.nextInt();
        s.close();
        int mainkor1 = koordinata1, mainkor2 = koordinata2;
        int kor1 = 0, kor2 = 0;
        for (int i = 1; i <= 64; i++) {
            board[mainkor1][mainkor2] = ++counter;
            int minacces = 8;
            int minA = 8, minB = 8;
            for (variant_hoda = 0; variant_hoda <= 7; variant_hoda++) {
                koordinata1 = mainkor1;
                koordinata2 = mainkor2;
                koordinata1 += gor[variant_hoda];
                koordinata2 += ver[variant_hoda];
                if (koordinata1 >= 0 && koordinata1 <= 7 && koordinata2 >= 0 && koordinata2 <= 7) {
                    acces[koordinata1][koordinata2]--;
                    if (minacces > acces[koordinata1][koordinata2] && board[koordinata1][koordinata2] == 0) {
                        minacces = acces[koordinata1][koordinata2];
                        if (board[koordinata1][koordinata2] == 0) {
                            kor1 = koordinata1;
                            kor2 = koordinata2;
                        }
                        int kor1A = koordinata1, kor2A = koordinata2;
                        for (int moveA = 0; moveA <= 7; moveA++) {
                            kor1A += gor[moveA];
                            kor2A += ver[moveA];
                            if (kor1A >= 0 && kor1A <= 7 && kor2A >= 0 && kor2A <= 7) {
                                if (minA >= acces[kor1A][kor2A] && board[kor1A][kor2A] == 0)
                                    minA = acces[kor1A][kor2A];
                            }
                        }
                    }
                    if (minacces == acces[koordinata1][koordinata2] && board[koordinata1][koordinata2] == 0) {
                        minacces = acces[koordinata1][koordinata2];
                        int kor1B = koordinata1, kor2B = koordinata2;
                        for (int moveB = 0; moveB <= 7; moveB++) {
                            kor1B += gor[moveB];
                            kor2B += ver[moveB];
                            if (kor1B >= 0 && kor1B <= 7 && kor2B >= 0 && kor2B <= 7) {
                                if (minB >= acces[kor1B][kor2B] && board[kor1B][kor2B] == 0)
                                    minB = acces[kor1B][kor2B];
                            }
                        }
                        if (board[koordinata1][koordinata2] == 0 && minB < minA) {
                            kor1 = koordinata1;
                            kor2 = koordinata2;
                        }
                    }
                }
            }
            mainkor1 = kor1;
            mainkor2 = kor2;
        }
        printBoard();
    }
    private static void printBoard() {
        System.out.format("\n");
        for (int j = 0; j < ver_razmer; j++) {
            for (int i = 0; i < gor_razmer; i++) {
                System.out.format("%4d", board[i][j]);
            }
            System.out.format("\n\n");
        }
    }
}
 
      