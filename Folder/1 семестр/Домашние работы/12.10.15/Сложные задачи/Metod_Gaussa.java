import java.util.Scanner;
 
public class Metod_Gaussa {
    public static void main(String[] args) {
        double[][] a;
        double[] y, x;
        int n;
 
        Scanner readData = new Scanner(System.in);
        System.out.print("������� ���������� ���������: ");
        n = readData.nextInt();
        a = new double[n][n];
        y = new double[n];
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                System.out.print("������� a[" + i +"][" + j + "] = ");
                a[i][j] = readData.nextInt();
            }
        }
        for (int i = 0; i < n; i++){
            System.out.print("������� y[" + i +"] = ");
            y[i] = readData.nextInt();
        }
        readData.close();
        printSystem(a, y, n);
        x = gauss(a, y, n);
        for (int i = 0; i < n; i++) System.out.println("x[" + i + "] = " + x[i]);
    }
 
 
 
    public static void printSystem(double[][] a, double[] y, int n){
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                System.out.print(a[i][j] + "*x" + j);
                if (j < n - 1) System.out.print(" + ");
            }
            System.out.println(" = " + y[i]);
        }
    } 
    public static double[] gauss(double[][] a, double[] y, int n){
        double[] x = new double[n];
        double max;
        int k = 0, index;
        final double EPS = 0.00001;
        while (k < n){
            max = Math.abs(a[k][k]);
            index = k;
            for (int i = k + 1; i < n; i++){
                if (Math.abs(a[i][k]) > max){
                    max = Math.abs(a[i][k]);
                    index = i;
                }
            }
            if (max < EPS){
                System.err.print("������� ���������� �������� ��-�� �������� ������� " + index + " ������� �.");
                System.exit(0);
            }
            for (int j = 0; j < n; j++){
                double temp = a[k][j];
                a[k][j] = a[index][j];
                a[index][j] = temp;
            }
            double temp = y[k];
            y[k] = y[index];
            y[index] = temp;
            for (int i = k; i < n; i++){
                temp = a[i][k];
                if (Math.abs(temp) < EPS) continue;
                for (int j = 0; j < n; j++) a[i][j] = a[i][j] / temp;
                y[i] = y[i] / temp;
                if (i == k) continue;
                for (int j = 0; j < n; j++) a[i][j] = a[i][j] - a[k][j];
                y[i] = y[i] - y[k];
            }
            k++;
        }
        for (k = n - 1; k >= 0; k--){
            x[k] = y[k];
            for (int i = 0; i < k; i++) y[i] = y[i] - a[i][k]*x[k];
        }
        return x;
    }
}