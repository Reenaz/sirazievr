from key_neighbor import key_neighbor
from dictionary import words


def check_neighbors(input_word):
    for word in words:
        if input_word.lower() == word:
            return input_word
        for index, char in enumerate(input_word):
            for charr in key_neighbor[char]:
                tmp = input_word[index]
                input_word = input_word[:index] + charr + input_word[index + 1:]
                if input_word.lower() == word:
                    return input_word
                else:
                    input_word = input_word[:index] + tmp + input_word[index + 1:]
    return input_word


def check_length(input_word):
    for word in words:
        if input_word.lower() == word:
            return input_word
